package Runnable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import Master.Field_Master;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Order_Wine extends Field_Master {
@BeforeTest
public void befortest() throws Exception, InterruptedException {
		driverset();
}
@Test(priority = 1, description = "Verify Back to Order Functionality")
public void Back_To_Order_Verify() throws Exception, Throwable, InterruptedException {
		row = 1;
		registerData();
		final_owv_clear();
		Address_Card();
		Thread.sleep(4000);
		login_button();
		Thread.sleep(1000);
		login_link();
		Thread.sleep(1000);
		useremailaddress();
		Thread.sleep(1000);
		userpassword();
		Thread.sleep(1000);
		login_now_button();
		Thread.sleep(2000);
		myaccountlink();
		Thread.sleep(1000);
		myprofielink();
		Thread.sleep(1000);
		ordertab();
		Thread.sleep(1000);
		orderwinebutton();
		Thread.sleep(1000);
		backtoorder();
		Thread.sleep(1000);
		String crturl = driver.getCurrentUrl();

		if(crturl.endsWith("userdashboard?t=1"))
		{
			getLogger().info("Back to Order option working properly");
			col = 6;
			row = 24;
			testresult = "Pass";
			validation_result();
		}
		else
		{
			getLogger().info("Back to Order option not working properly");
			col = 6;
			row = 24;
			testresult = "Fail"; 
			validation_result(); 
		}
		Thread.sleep(1000);
		orderwinebutton();
		Thread.sleep(1000);
	}
@Test(priority = 2, description = "Add Product to Cart")
public void Add_Product_Cart() throws Exception, Throwable, InterruptedException {
		addbottle1();
		Thread.sleep(1000);
		addbottle2();
		Thread.sleep(1000);
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cart1stbottle"))));
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cart2ndbottle"))));
			getLogger().info("Add Bottle to cart verify successfully");
			col = 6;
			row = 25;
			testresult = "Pass"; 
			validation_result(); 
		} catch (Exception e) {
			getLogger().info("Add Bottle to cart not verify");
			col = 6;
			row = 25;
			testresult = "Fail"; 
			validation_result(); 
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}		
}
@Test(priority = 3, description = "Increased Bottle Quantity")
public void Increase_Bottle_QTY() throws Exception, Throwable, InterruptedException {
		
		try 
		{
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("qty_plus1"))));
			getDriver().findElement(By.xpath(getObj().getProperty("qty_plus1"))).click();
			Thread.sleep(500);
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("qty_plus2"))));
			getDriver().findElement(By.xpath(getObj().getProperty("qty_plus2"))).click();
			Thread.sleep(500);
			getLogger().info("Bottle QTY Added to Cart");
			
		} catch (Exception e) {
			getLogger().info("Bottle Add QTY Button not working"); 
			screenshotname = "Bottle_Add_QTY_Button_Not_working";
			getscreenshot();
			Assert.fail();
		}	
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("qty_box1"))));
			String qty1 = getDriver().findElement(By.xpath(getObj().getProperty("qty_box1"))).getAttribute("value");

			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("qty_box2"))));
			String qty2 = getDriver().findElement(By.xpath(getObj().getProperty("qty_box2"))).getAttribute("value");

			if(qty1.contentEquals("2") && qty2.contentEquals("2"))
			{
				getLogger().info("Increased Cart Product Quantity Verify Successfull"); 
				col = 6;
				row = 26;
				testresult = "Pass"; 
				validation_result(); 
			}
			else
			{
				getLogger().info("Increased Cart Product Quantity not Verify"); 
				col = 6;
				row = 26;
				testresult = "Fail"; 
				validation_result();
				Assert.fail();
			}
		} catch (Exception e) {
			TestReason = "Increased Cart Product Quantity not Verify";
			getLogger().info(TestReason);
			col = 6;
			row = 26;
			testresult = "Fail"; 
			validation_result();
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}
}
@Test(priority = 4, description = "Decreased Bottle Quantity")
public void Decrease_Bottle_QTY() throws Exception, Throwable, InterruptedException {
		
		try 
		{
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("qty_minus1"))));
			getDriver().findElement(By.xpath(getObj().getProperty("qty_minus1"))).click();
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("qty_minus2"))));
			getDriver().findElement(By.xpath(getObj().getProperty("qty_minus2"))).click();
			getLogger().info("Bottle QTY Decrease from Cart");
		} catch (Exception e) {
			TestReason = "Bottle Decrease QTY Button not working";
			getLogger().info(TestReason);
			screenshotname = "Bottle_Decrease_QTY_Button_Not_working";
			getscreenshot();
			Assert.fail();
		}
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("qty_box1"))));
			String qty1 = getDriver().findElement(By.xpath(getObj().getProperty("qty_box1"))).getAttribute("value");

			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("qty_box2"))));
			String qty2 = getDriver().findElement(By.xpath(getObj().getProperty("qty_box2"))).getAttribute("value");
			
			if(qty1.contentEquals("1") && qty2.contentEquals("1"))
			{
				getLogger().info("Decreased Cart Product Quantity Verify Successfull"); 
				col = 6;
				row = 27;
				testresult = "Pass"; 
				validation_result(); 
			}
			else
			{
				getLogger().info("Decreased Cart Product Quantity not Verify"); 
				col = 6;
				row = 27;
				testresult = "Fail"; 
				validation_result(); 
			}
		} catch (Exception e) {
			getLogger().info("Decreased Cart Product Quantity not Verify"); 
			col = 6;
			row = 27;
			testresult = "Fail"; 
			validation_result(); 
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}
}
@Test(priority = 5, description = "Remove Bottle Quantity")
public void Remove_Bottle_QTY() throws Exception, Throwable, InterruptedException {	
		try 
		{
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Remove_bottle1"))));
			getDriver().findElement(By.xpath(getObj().getProperty("Remove_bottle1"))).click();
			Thread.sleep(500);
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Remove_bottle1"))));
			getDriver().findElement(By.xpath(getObj().getProperty("Remove_bottle1"))).click();
			getLogger().info("Bottle QTY Removed from Cart");
		} catch (Exception e) {
			TestReason = "Bottle QTY Button not working";
			getLogger().info(TestReason);
			screenshotname = "Bottle_QTY_Button_Not_working";
			getscreenshot();
			Assert.fail();
		}
		Thread.sleep(1000);
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("empty_cart"))));
			String qtyrm = getDriver().findElement(By.xpath(getObj().getProperty("empty_cart"))).getText();	
			if(qtyrm.contentEquals("The box is empty."))
			{
				getLogger().info("Removed Cart Quantity Verify Successfull"); 
				col = 6;
				row = 28;
				testresult = "Pass"; 
				validation_result();
			}
			else
			{
				getLogger().info("Removed Cart Quantity not Verify"); 
				col = 6;
				row = 28;
				testresult = "Fail"; 
				validation_result();
			}
		} catch (Exception e) {
			TestReason = "Removed Cart Quantity not Verify";
			getLogger().info(TestReason);
			col = 6;
			row = 28;
			testresult = "Fail"; 
			validation_result();
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}
}
@Test(priority = 6, description = "Cart Quantitiy Verification Total")
public void Verify_Cart_QTY_Total() throws Exception, Throwable, InterruptedException {
		Thread.sleep(1000);
		addbottle1();
		Thread.sleep(1000);
		addbottle2();
		Thread.sleep(1000);
		addbottle1();
		Thread.sleep(1000);
		addbottle2();
		Thread.sleep(1000);
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("total_qty"))));
			String itemqty = getDriver().findElement(By.xpath(getObj().getProperty("total_qty"))).getText();
			
			if(itemqty.contentEquals("4"))
			{
				getLogger().info("Cart Quantity Total Verify Successfull"); 
				col = 6;
				row = 29;
				testresult = "Pass"; 
				validation_result();
			}
			else
			{
				getLogger().info("Cart Quantity Total not Verify"); 
				col = 6;
				row = 29;
				testresult = "Fail"; 
				validation_result();
			}
		} catch (Exception e) {
			getLogger().info("Cart Quantity Total not Verify"); 
			col = 6;
			row = 29;
			testresult = "Fail"; 
			validation_result();
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}
}
@Test(priority = 7, description = "Order Placed With Existing Account Details")
public void Order_Place_With_Existing_Details() throws Exception, Throwable, InterruptedException {
		Thread.sleep(1000);
		placeorderbutton();
		Thread.sleep(1000);
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("order_success"))));
			String itemqty = getDriver().findElement(By.xpath(getObj().getProperty("order_success"))).getText();			
			if(itemqty.contentEquals("Order placed successfully!"))
			{
				getLogger().info("Order Placed With Existing Account Details verify successfull"); 
				col = 6;
				row = 30;
				testresult = "Pass"; 
				validation_result();
			}
			else
			{
				getLogger().info("Order Placed With Existing Account Details not verify"); 
				col = 6;
				row = 30;
				testresult = "Fail"; 
				validation_result();
			}		
		} catch (Exception e) {
			getLogger().info("Order Placed With Existing Account Details not verify"); 
			col = 6;
			row = 30;
			testresult = "Fail"; 
			validation_result();
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}
}

@Test (priority = 8)
public void Address1_Validate() throws Exception, Throwable,InterruptedException {
			Thread.sleep(2000);
			orderwinebutton();
			addbottle1();
			Thread.sleep(1000);
			addbottle2();
			Thread.sleep(1000);
			try 
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cart1stbottle"))));
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cart2ndbottle"))));
				getLogger().info("Add Bottle to cart verify successfully");
			} catch (Exception e) {
				getLogger().info("Add Bottle to cart not verify");
				screenshotname = "Bottle_Not_Found_In_Cart";
				getscreenshot();
				Assert.fail();
			}		
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("add_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("add_address_button"))).click();
				getLogger().info("Add New Address option open to enter");	
			}
			catch(Exception e)
			{
				TestReason = "Add New Address option not open.";
				getLogger().info(TestReason);
				screenshotname = "Add_Address_option_button_not_found";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without Address 1 Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
			String address1_blank = getDriver().findElement(By.xpath(getObj().getProperty("address1_blank"))).getText();
			
			if(address1_blank.equals("Address is required"))
			{
				getLogger().info("Address1 Validation Pass");
				col = 6;
				row = 31;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Address1 Validation Fail");
				col = 6;
				row = 31;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("order_address"))));
				getDriver().findElement(By.xpath(getObj().getProperty("order_address"))).sendKeys(streetaddress1);
				getLogger().info("Address1 Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Address1 field not found";
				getLogger().info(TestReason);
				screenshotname = "Address1_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);}			
@Test (priority = 9)
public void City_Validate() throws Exception, Throwable,InterruptedException {
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without Address 1 Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String cityblank = getDriver().findElement(By.xpath(getObj().getProperty("city_blank"))).getText();
			
			if(cityblank.equals("City is required"))
			{
				getLogger().info("City Validation Pass");
				col = 6;
				row = 32;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("City Validation Fail");
				col = 6;
				row = 32;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("order_city"))));
				getDriver().findElement(By.xpath(getObj().getProperty("order_city"))).sendKeys(city);
				getLogger().info("City Inserted");
			}
			catch(Exception e)
			{
				TestReason = "City field not found";
				getLogger().info(TestReason);
				screenshotname = "City_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
}			
@Test(priority = 10)
public void State_Validate() throws Exception, Throwable,InterruptedException {			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without State Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String stateblank = getDriver().findElement(By.xpath(getObj().getProperty("state_blank"))).getText();
			
			if(stateblank.equals("Region is required"))
			{
				getLogger().info("State Validation Pass");
				col = 6;
				row = 33;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("State Validation Fail");
				col = 6;
				row = 33;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("state"))));
				getDriver().findElement(By.xpath(getObj().getProperty("state"))).click();
			    WebElement dropdownValue = driver.findElement(By.xpath("//div[contains(text(),'"+state+"')]"));
			    dropdownValue.click();
				getLogger().info("State Selected");
			}
			catch(Exception e)
			{
				TestReason = "State field not found";
				getLogger().info(TestReason);
				screenshotname = "State_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
}
@Test(priority = 11)
public void Address_Zipcode_Validate() throws Exception, Throwable,InterruptedException {
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without Zipcode Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String zipcodeblank = getDriver().findElement(By.xpath(getObj().getProperty("zipcode_blank"))).getText();
			
			if(zipcodeblank.equals("Postal is required"))
			{
				getLogger().info("Zipcode Validation Pass");
				col = 6;
				row = 34;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Zipcode Validation Fail");
				col = 6;
				row = 34;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("order_zip"))));
				getDriver().findElement(By.xpath(getObj().getProperty("order_zip"))).sendKeys(zip);
				getLogger().info("Zip Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zip field not found";
				getLogger().info(TestReason);
				screenshotname = "Zip_Field_Not_Found";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Address Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
		}
@Test (priority = 12)
public void Credit_Card_Name_Validate() throws Exception, Throwable,InterruptedException {
			Thread.sleep(1000);
			replacecard();
			Thread.sleep(1000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String cardnameblank = getDriver().findElement(By.xpath(getObj().getProperty("cardname_blank"))).getText();
			
			if(cardnameblank.equals("This field is required"))
			{
				getLogger().info("Require Card Name Validation Pass");
				col = 6;
				row = 35;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Require Card Name Validation Fail");
				col = 6;
				row = 35;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardname"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardname"))).sendKeys(cardname);
				getLogger().info("Cardname Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Cardname field not found";
				getLogger().info(TestReason);
				getscreenshot();
				screenshotname = "Cardname_Field_Not_Found";
			}
			Thread.sleep(1000);
}
@Test (priority = 13)
public void Credit_Card_Number_Validate() throws Exception, Throwable,InterruptedException {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String cardnumberblank = getDriver().findElement(By.xpath(getObj().getProperty("cardnumber_blank"))).getText();
			
			if(cardnumberblank.equals("This field is required"))
			{
				getLogger().info("Require Card Number Validation Pass");
				col = 6;
				row = 36;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Require Card Number Validation Fail");
				col = 6;
				row = 36;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardnumber"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).sendKeys("123");
				getLogger().info("Invalid Card Number Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Card Number field not found";
				getLogger().info(TestReason);
				screenshotname = "Card_Number_Field_Not_Found";
				getscreenshot();
			}
			String cardnumberinvalid = getDriver().findElement(By.xpath(getObj().getProperty("cardnumber_invalid"))).getText();
			
			if(cardnumberinvalid.equals("Enter valid card number"))
			{
				getLogger().info("Invalid Card Number Validation Pass");
				col = 6;
				row = 37;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Invalid Card Number Validation Fail");
				col = 6;
				row = 37;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardnumber"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).sendKeys(cardnumber);
				getLogger().info("Card Number Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Card Number field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardnumber_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
			
}
@Test (priority = 14)
public void Exp_Date_Validate() throws Exception, Throwable,InterruptedException {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String expdateblank = getDriver().findElement(By.xpath(getObj().getProperty("expdate_blank"))).getText();
			
			if(expdateblank.equals("This field is required"))
			{
				getLogger().info("Require Exp Date Validation Pass");
				col = 6;
				row = 38;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Require Exp Date Validation Fail");
				col = 6;
				row = 38;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("expdate"))));
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).sendKeys("abc");
				getLogger().info("Invalid Exp Date Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Exp Date field not found";
				getLogger().info(TestReason);
				screenshotname = "Exp_Date_Field_Not_Found";
				getscreenshot();
			}
			String expdateinvalid = getDriver().findElement(By.xpath(getObj().getProperty("expdate_invalid"))).getText();
			
			if(expdateinvalid.equals("Enter valid expiry date"))
			{
				getLogger().info("Invalid Exp Date Validation Pass");
				col = 6;
				row = 39;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Invalid Exp Date Validation Fail");
				col = 6;
				row = 39;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("expdate"))));
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).sendKeys(expdate);
				getLogger().info("Exp Date Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Exp Date field not found";
				getLogger().info(TestReason);
				screenshotname = "ExpDate_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
			
}
@Test (priority = 15)
public void CVV_Validate() throws Exception, Throwable,InterruptedException {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String cvvblank = getDriver().findElement(By.xpath(getObj().getProperty("cvv_blank"))).getText();
	
			if(cvvblank.equals("Enter valid cvv"))
			{
				getLogger().info("Require CVV Validation Pass");
				col = 6;
				row = 40;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Require CVV Validation Fail");
				col = 6;
				row = 40;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cvv"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).sendKeys("abcd");
				getLogger().info("Invalid CVV Inserted");
			}
			catch(Exception e)
			{
				TestReason = "CVV field not found";
				getLogger().info(TestReason);
				screenshotname = "CVV_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
			}
			String cvvinvalid = getDriver().findElement(By.xpath(getObj().getProperty("cvv_blank"))).getText();
			
			if(cvvinvalid.equals("Enter valid cvv"))
			{
				getLogger().info("Invalid CVV Validation Pass");
				col = 6;
				row = 41;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Invalid CVV Validation Fail");
				col = 6;
				row = 41;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cvv"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).sendKeys(cvv);
				getLogger().info("Valid CVV Inserted");
			}
			catch(Exception e)
			{
				TestReason = "CVV field not found";
				getLogger().info(TestReason);
				screenshotname = "CVV_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(1000);
}
@Test (priority = 16)
public void Bill_Zip_Validate() throws Exception, Throwable,InterruptedException {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String billzipblank = getDriver().findElement(By.xpath(getObj().getProperty("bill_zip_blank"))).getText();
	
			if(billzipblank.equals("This field is required"))
			{
				getLogger().info("Require Bill Zip Validation Pass");
				col = 6;
				row = 42;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Require Bill Zip Validation Fail");
				col = 6;
				row = 42;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zipcode"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).sendKeys("123");
				getLogger().info("Invalid Zipcode Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zipcode field not found";
				getLogger().info(TestReason);
				screenshotname = "Zipcode_Field_Not_Found";
				getscreenshot();
			}
			String billzipinvalid = getDriver().findElement(By.xpath(getObj().getProperty("bill_zip_blank"))).getText();
			
			if(billzipinvalid.equals("This field is required"))
			{
				getLogger().info("Invalid Bill Zip Validation Pass");
				col = 6;
				row = 43;
				testresult = "Pass";
				validation_result();
			}
			else
			{
				getLogger().info("Invalid Bill Zip Validation Fail");
				col = 6;
				row = 43;
				testresult = "Fail";
				validation_result();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zipcode"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).sendKeys(zipcode);
				getLogger().info("Zipcode Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zipcode field not found";
				getLogger().info(TestReason);
				screenshotname = "Zipcode_Field_Not_Found";
				getscreenshot();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			Thread.sleep(2000);
}
@Test(priority = 17, description = "Order Placed With New Account Details")
public void Order_Place_With_New_Details() throws Exception, Throwable, InterruptedException {
		Thread.sleep(1000);
		placeorderbutton();
		Thread.sleep(2000);
		try 
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("order_success"))));
			String itemqty = getDriver().findElement(By.xpath(getObj().getProperty("order_success"))).getText();			
			if(itemqty.contentEquals("Order placed successfully!"))
			{
				getLogger().info("Order Placed With Existing Account Details verify successfull"); 
				col = 6;
				row = 44;
				testresult = "Pass"; 
				validation_result();
			}
			else
			{
				getLogger().info("Order Placed With Existing Account Details not verify"); 
				col = 6;
				row = 44;
				testresult = "Fail"; 
				validation_result();
			}		
		} catch (Exception e) {
			getLogger().info("Order Placed With Existing Account Details not verify"); 
			col = 6;
			row = 30;
			testresult = "Fail"; 
			validation_result();
			screenshotname = "Bottle_Not_Found_In_Cart";
			getscreenshot();
			Assert.fail();
		}
}
@AfterTest
public void aftertest() throws InterruptedException {
		getDriver().quit();
}
}