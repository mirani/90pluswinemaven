package Runnable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import com.google.common.base.CharMatcher;
import Master.Field_Master;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class Account_Setting extends Field_Master {
	
@BeforeTest
public void befortest() throws Exception,InterruptedException {
		     driverset();	
}	

@Test(priority = 1, description = "Profile Info - Existing Details Verifcation")
public void Existing_Data_Verify() throws Exception, Throwable,InterruptedException {
			row = 1;
			accountsettingdata();
			account_Setting_result_clear();
			final_asv_clear();
			registerData();
			Thread.sleep(4000);
			login_button();
			Thread.sleep(1000);
			login_link();
			Thread.sleep(1000);
			useremailaddress();
			Thread.sleep(1000);
			userpassword();			
			Thread.sleep(1000);
			login_now_button();
			Thread.sleep(2000);
			myaccountlink();
			Thread.sleep(1000);
			accoutsettinglink();
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("existing_name_field"))));
				String existingfullname = driver.findElement(By.xpath(getObj().getProperty("existing_name_field"))).getAttribute("value");
				getLogger().info(existingfullname);
				
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("existing_email_field"))));
				String existingemail = driver.findElement(By.xpath(getObj().getProperty("existing_email_field"))).getAttribute("value");
				getLogger().info(existingemail);
				
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("existing_mobile_field"))));
				String theDigits = driver.findElement(By.xpath(getObj().getProperty("existing_mobile_field"))).getAttribute("value");
				@SuppressWarnings("deprecation")
				String existingmobile = CharMatcher.digit().retainFrom(theDigits);
				getLogger().info(existingmobile);
				
				if (existingfullname.contentEquals(fullname) && existingemail.contentEquals(email) && existingmobile.contentEquals(mobile)) {
					getLogger().info("Existing Profile Info Verify Successfully");
					col = 2;
					row = 17;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Existing Profile Info Not Verify.");
					col = 2;
					row = 17;
					testresult = "Fail";
					validation_result();
				}
				
			} catch (Exception e) {
				TestReason = "Existing Profile Info Not Verify.";
				getLogger().info(TestReason);
				screenshotname = "Existing_Profile_Info_Invalid";
				getscreenshot();
				col = 2;
				row = 17;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(3000);
}
@Test (priority = 2 , enabled = true, description = "Profile Info - Mandatory Field Validation")
public void Profile_Info_Mandatory_Validation() throws Exception, Throwable,InterruptedException {

			row = 1;
			accountsettingdata();
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("edit_profile_info_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("edit_profile_info_button"))).click();
				getLogger().info("Edit Profile Info Button Click");
			} catch (Exception e) {
				TestReason = "Edit Profile Info button not found";
				getLogger().info(TestReason);
				screenshotname = "Edit_Profile_Info_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("existing_name_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).clear();
				
				/*getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("existing_email_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("existing_email_field"))).clear();
				
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("existing_mobile_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("existing_mobile_field"))).clear();*/
									
			} catch (Exception e) {
				TestReason = "Existing Profile Info Not Verify.";
				getLogger().info(TestReason);
				screenshotname = "Existing_Profile_Info_Invalid";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("save_profile_info_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_profile_info_button"))).click();
				getLogger().info("Save Profile Info Button Click");
			} catch (Exception e) {
				TestReason = "Save Profile Info button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Profile_Info_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("fullnamerequiremsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("fullnamerequiremsg"))).getText();
				
				if (currentpassword.contentEquals("This field is required.")) {
					getLogger().info("Full Name Mandatory Validation Verify Successfully");
					col = 2;
					row = 23;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Full Name Mandatory Validation Not Verify");
					col = 2;
					row = 23;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Full Name Mandatory Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Full_Name_Mandatory_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 23;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			/*try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("emailrequiremsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("emailrequiremsg"))).getText();
				
				if (currentpassword.contentEquals("This field is required.")) {
					getLogger().info("Email Address Mandatory Validation Verify Successfully");
					col = 2;
					row = 24;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Email Address Mandatory Validation Not Verify");
					col = 2;
					row = 24;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Email Address Mandatory Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Email_Address_Mandatory_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 24;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobilerequiremsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("mobilerequiremsg"))).getText();
				
				if (currentpassword.contentEquals("This field is required.")) {
					getLogger().info("Mobile Number Mandatory Validation Verify Successfully");
					col = 2;
					row = 25;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Mobile Number Mandatory Validation Not Verify");
					col = 2;
					row = 25;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Mobile Number Mandatory Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Mobile_Number_Mandatory_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 25;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}*/
			
			Thread.sleep(1000);
}
@Test (priority = 3 , enabled = true, description = "Profile Info - Invalid Email Address Validation")
public void Invalid_Email_Address_Validation() throws Exception, Throwable,InterruptedException {
	
			row = 1;
			accountsettingdata();
			String email = "test@124";
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("existing_name_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).sendKeys(updatefullname);
				getLogger().info("Full Name Inserted");
			} catch (Exception e) {
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "Full_Name_field_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("new_email_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("new_email_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("new_email_field"))).sendKeys(email);
				getLogger().info("Email Address Inserted");
			} catch (Exception e) {
				TestReason = "Email Address field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Address_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 18;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("save_profile_info_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_profile_info_button"))).click();
				getLogger().info("Save Profile Info Button Click");
			} catch (Exception e) {
				TestReason = "Save Profile Info Button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Profle_Info_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("invalidnewemailmsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("invalidnewemailmsg"))).getText();
				
				if (currentpassword.contentEquals("Email address is invalid")) {
					getLogger().info("Invalid Email Adddress Validation Verify Successfully");
					col = 2;
					row = 18;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Invalid Email Address Validation Not Verify");
					col = 2;
					row = 18;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Invalid Email Address Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Invalid_Email_Address_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 18;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 4 , enabled = true, description = "Profile Info - Invalid Mobile Number Validation")
public void Invalid_Mobile_Validation() throws Exception, Throwable,InterruptedException {
	
			row = 1;
			accountsettingdata();
			String mobile = "12345";
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("existing_name_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).sendKeys(updatefullname);
				getLogger().info("Full Name Inserted");
			} catch (Exception e) {
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "Full_Name_field_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("new_email_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("new_email_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("new_email_field"))).sendKeys(updateemail);
				getLogger().info("Email Address Inserted");
			} catch (Exception e) {
				TestReason = "Email Address field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Address_field_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("new_mobile_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("new_mobile_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("new_mobile_field"))).sendKeys(mobile);
				getLogger().info("Mobile Number Inserted");
			} catch (Exception e) {
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 19;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("save_profile_info_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_profile_info_button"))).click();
				getLogger().info("Save Profile Info Button Click");
			} catch (Exception e) {
				TestReason = "Save Profile Info Button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Profle_Info_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("invalidnewmobilemsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("invalidnewmobilemsg"))).getText();
				
				if (currentpassword.contentEquals("Mobile number must contain 10 digits.")) {
					getLogger().info("Invalid Mobile number Validation Verify Successfully");
					col = 2;
					row = 19;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Invalid Mobile number Validation Not Verify");
					col = 2;
					row = 19;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Invalid Mobile number Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Invalid_Mobile_Number_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 19;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 5 , enabled = true, description = "Profile Info - Profile Details Save Successful Verification")
public void Profile_Info_Save_Success() throws Exception, Throwable,InterruptedException {
	
			row = 1;
			accountsettingdata();
			updateemail = todaysdate + "@mailinator.com";
			updatemobile = todaysdate;
			setaccountsettingdetails();
			
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("existing_name_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("existing_name_field"))).sendKeys(updatefullname);
				getLogger().info("Full Name Inserted");
			} catch (Exception e) {
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "Full_Name_field_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("new_email_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("new_email_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("new_email_field"))).sendKeys(updateemail);
				getLogger().info("Email Address Inserted");
			} catch (Exception e) {
				TestReason = "Email Address field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Address_field_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("new_mobile_field"))));
				getDriver().findElement(By.xpath(getObj().getProperty("new_mobile_field"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("new_mobile_field"))).sendKeys(updatemobile);
				getLogger().info("Mobile Number Inserted");
			} catch (Exception e) {
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 29;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("save_profile_info_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_profile_info_button"))).click();
				getLogger().info("Save Profile Info Button Click");
			} catch (Exception e) {
				TestReason = "Save Profile Info Button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Profle_Info_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("profileinfosuccessmsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("profileinfosuccessmsg"))).getText();
				
				if (currentpassword.contentEquals("Profile Info updated successfully")) {
					getLogger().info("Profile Info Update Verify Successfully");
					col = 2;
					row = 29;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Profile Info Update Not Verify");
					col = 2;
					row = 29;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Profile Info Update Failed";
				getLogger().info(TestReason);
				screenshotname = "Profile_Info_Update_Failed";
				getscreenshot();
				col = 2;
				row = 29;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 6 , enabled = true, description = "Change Password - Mandatory Field Validation")
public void Change_Password_Mandatory() throws Exception, Throwable,InterruptedException {
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("change_password_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("change_password_link"))).click();
				getLogger().info("Change Password Link Click");
			} catch (Exception e) {
				TestReason = "Change Password Link not found";
				getLogger().info(TestReason);
				screenshotname = "Change_Password_Link_Not_Found";
				getscreenshot();
				Assert.fail();
			}	
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("change_password_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("change_password_button"))).click();
				getLogger().info("Change Password Button Click");
			} catch (Exception e) {
				TestReason = "Change Password Button not found";
				getLogger().info(TestReason);
				screenshotname = "Change_Password_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("currentpwdrequiremsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("currentpwdrequiremsg"))).getText();
				
				if (currentpassword.contentEquals("This field is required.")) {
					getLogger().info("Current Password Mandatory Validation Verify Successfully");
					col = 2;
					row = 24;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Current Password Mandatory Validation Not Verify");
					col = 2;
					row = 24;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Current Password Mandatory Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Current_Password_Mandatory_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 24;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(300);
			try {			
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("newpwdrequiremsg"))));
				String newpassword = driver.findElement(By.xpath(getObj().getProperty("newpwdrequiremsg"))).getText();				
				if (newpassword.contentEquals("This field is required.")) {
					getLogger().info("New Password Mandatory Validation Verify Successfully");
					col = 2;
					row = 25;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("New Password Mandatory Validation Not Verify");
					col = 2;
					row = 25;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "New Password Mandatory Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "New_Password_Mandatory_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 25;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(300);
			try {			
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("confirmpwdrequiremsg"))));
				String confirmpassword = driver.findElement(By.xpath(getObj().getProperty("confirmpwdrequiremsg"))).getText();				
				if (confirmpassword.contentEquals("This field is required.")) {
					getLogger().info("Confirm Password Mandatory Validation Verify Successfully");
					col = 2;
					row = 26;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Confirm Password Mandatory Validation Not Verify");
					col = 2;
					row = 26;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Confirm Password Mandatory Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Confirm_Password_Mandatory_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 26;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 7 , enabled = true, description = "Change Password - Invalid Password Validation")
public void Invalid_All_Password_Validation() throws Exception, Throwable,InterruptedException {
			String temppwd = "Test";
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("currentpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).sendKeys(temppwd);
				getLogger().info("Current Password Inserted");
			} catch (Exception e) {
				TestReason = "Current Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Current_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 20;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("newpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).sendKeys(temppwd);
				getLogger().info("New Password Inserted");
			} catch (Exception e) {
				TestReason = "New Password field not found";
				getLogger().info(TestReason);
				screenshotname = "New_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 21;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("confirmpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).sendKeys(temppwd);
				getLogger().info("Confirm Password Inserted");
			} catch (Exception e) {
				TestReason = "Confirm Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Confirm_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 22;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(1000);

			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("change_password_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("change_password_button"))).click();
				getLogger().info("Change Password Button Click");
			} catch (Exception e) {
				TestReason = "Change Password Button not found";
				getLogger().info(TestReason);
				screenshotname = "Change_Password_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("invalidcurrentpwdmsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("invalidcurrentpwdmsg"))).getText();
				
				if (currentpassword.contentEquals("Your password must be a minimum of 6 characters.")) {
					getLogger().info("Invalid Current Password Validation Verify Successfully");
					col = 2;
					row = 20;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Invalid Current Password Validation Not Verify");
					col = 2;
					row = 20;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Invalid Current Password Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Invalid_Current_Password_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 20;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {			
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("invalidnewpwdmsg"))));
				String newpassword = driver.findElement(By.xpath(getObj().getProperty("invalidnewpwdmsg"))).getText();
				
				if (newpassword.contentEquals("Your password must be a minimum of 6 characters.")) {
					getLogger().info("Invalid New Password Validation Verify Successfully");
					col = 2;
					row = 21;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Invalid New Password Validation Not Verify");
					col = 2;
					row = 21;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Invalid New Password Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Invalid_New_Password_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 21;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {			
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("invalidconfirmpwdmsg"))));
				String confirmpassword = driver.findElement(By.xpath(getObj().getProperty("invalidconfirmpwdmsg"))).getText();
				
				if (confirmpassword.contentEquals("Your password must be a minimum of 6 characters.")) {
					getLogger().info("Invalid Confirm Password Validation Verify Successfully");
					col = 2;
					row = 22;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Invalid Confirm Password Validation Not Verify");
					col = 2;
					row = 22;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Invalid Confirm Password Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Invalid_Confirm_Password_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 22;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 8 , enabled = true, description = "Change Password - Invalid Current Password Validation")
public void Invalid_Current_Password_Validation() throws Exception, Throwable,InterruptedException {
	
			String currentpwd = "99999999999";
			String newpwd = "Test@123";
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("currentpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).sendKeys(currentpwd);
				getLogger().info("Current Password Inserted");
			} catch (Exception e) {
				TestReason = "Current Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Current_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 27;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("newpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).sendKeys(newpwd);
				getLogger().info("New Password Inserted");
			} catch (Exception e) {
				TestReason = "New Password field not found";
				getLogger().info(TestReason);
				screenshotname = "New_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 27;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("confirmpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).sendKeys(newpwd);
				getLogger().info("Confirm Password Inserted");
			} catch (Exception e) {
				TestReason = "Confirm Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Confirm_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 27;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("change_password_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("change_password_button"))).click();
				getLogger().info("Change Password Button Click");
			} catch (Exception e) {
				TestReason = "Change Password Button not found";
				getLogger().info(TestReason);
				screenshotname = "Change_Password_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("wrongcurrentpwdmsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("wrongcurrentpwdmsg"))).getText();
				
				if (currentpassword.contentEquals("Incorrect current password.")) {
					getLogger().info("Wrong Current Password Validation Verify Successfully");
					col = 2;
					row = 27;
					testresult = "Pass";
					validation_result();
					System.out.println("Result has been written successfully");
				} else {
					getLogger().info("Wrong Current Password Validation Not Verify");
					col = 2;
					row = 27;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Wrong Current Password Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Wrong_Password_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 27;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 9 , enabled = true, description = "Change Password - New & Confirm Password Mismatch Validation")
public void New_Confirm_Password_Mismatch_Validation() throws Exception, Throwable,InterruptedException {
	
			String currentpwd = "Test@123";
			String newpwd = "Test@123";
			String confirmpwd = "Good2go";
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("currentpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).sendKeys(currentpwd);
				getLogger().info("Current Password Inserted");
			} catch (Exception e) {
				TestReason = "Current Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Current_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 28;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("newpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).sendKeys(newpwd);
				getLogger().info("New Password Inserted");
			} catch (Exception e) {
				TestReason = "New Password field not found";
				getLogger().info(TestReason);
				screenshotname = "New_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 28;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("confirmpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).sendKeys(confirmpwd);
				getLogger().info("Confirm Password Inserted");
			} catch (Exception e) {
				TestReason = "Confirm Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Confirm_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 28;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("change_password_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("change_password_button"))).click();
				getLogger().info("Change Password Button Click");
			} catch (Exception e) {
				TestReason = "Change Password Button not found";
				getLogger().info(TestReason);
				screenshotname = "Change_Password_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mismatchpwdmsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("mismatchpwdmsg"))).getText();
				
				if (currentpassword.contentEquals("Password does not match with newPassword.")) {
					getLogger().info("Password Mismatch Validation Verify Successfully");
					col = 2;
					row = 28;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Password Mismatch Validation Not Verify");
					col = 2;
					row = 28;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Password Mismatch Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Password_Mismatch_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 28;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@Test (priority = 10 , enabled = true, description = "Change Password - Password Changed Successful Verfication")
public void Password_Change_Successfully() throws Exception, Throwable,InterruptedException {
			row = 1;
			registerData();
			accountsettingdata();
			
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("currentpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("currentpassword"))).sendKeys(password);
				getLogger().info("Current Password Inserted");
			} catch (Exception e) {
				TestReason = "Current Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Current_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 30;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("newpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("newpassword"))).sendKeys(updatepassword);
				getLogger().info("New Password Inserted");
			} catch (Exception e) {
				TestReason = "New Password field not found";
				getLogger().info(TestReason);
				screenshotname = "New_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 30;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("confirmpassword"))));
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).clear();
				Thread.sleep(300);
				getDriver().findElement(By.xpath(getObj().getProperty("confirmpassword"))).sendKeys(updatepassword);
				getLogger().info("Confirm Password Inserted");
			} catch (Exception e) {
				TestReason = "Confirm Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Confirm_Password_field_Not_Found";
				getscreenshot();
				col = 2;
				row = 30;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("change_password_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("change_password_button"))).click();
				getLogger().info("Change Password Button Click");
			} catch (Exception e) {
				TestReason = "Change Password Button not found";
				getLogger().info(TestReason);
				screenshotname = "Change_Password_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
			Thread.sleep(1000);
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("pwdchangesuccessmsg"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("pwdchangesuccessmsg"))).getText();
				
				if (currentpassword.contentEquals("Password changed successfully")) {
					getLogger().info("Password changed successfull.");
					col = 2;
					row = 30;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Password changed not successfull.");
					col = 2;
					row = 30;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Password Change Validation Failed";
				getLogger().info(TestReason);
				screenshotname = "Password_Change_Validation_Failed";
				getscreenshot();
				col = 2;
				row = 30;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
}
@AfterTest
public void aftertest() throws InterruptedException {
   getDriver().quit();
}
	}