package Master;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import Master.DataCall;

public class Field_Master extends DataCall{
	
//------------------------------------------------Register-----------------------------------------------------------------	
		public void signup() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up"))).click();
				getLogger().info("Sign_Up Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign_Up Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Opened";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();
			}
		}
		public void age_verify() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("age_verify"))));
				getDriver().findElement(By.xpath(getObj().getProperty("age_verify"))).click();
				getLogger().info("Age verification successfully done.");
			}
			catch(Exception e)
			{
				TestReason = "Age verification not done.";
				getLogger().info(TestReason);
				screenshotname = "Age_Verification_Not_Done";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();		
			}
		}
		public void signup_link() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath(getObj().getProperty("sign_up_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up_link"))).click();
				getLogger().info("Sign Up link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up link not found.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Found";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();		
			}
		}
		public void userfullname() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
				getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).sendKeys(fullname);
				getLogger().info("Full Name Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "FullName_Field_Not_Found";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();			
			}
		}
		public void useremailaddress() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).clear();
				Thread.sleep(1000);	
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found";
				getscreenshot();
			}	
		}
		public void userdateemailaddress() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(dateemail);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();
			}	
		}
		public void updateduseremailaddress() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).clear();
				Thread.sleep(1000);	
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(updateemail);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found";
				getscreenshot();
			}	
		}
		public void usermobile() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
				getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(mobile);
				getLogger().info("Mobile No Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_Field_Not_Found";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();
			}
		}
		public void userdatemobile() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
				getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(datemobile);
				getLogger().info("Mobile No Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_Field_Not_Found";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();
			}
		}
		public void userpassword() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).clear();
				Thread.sleep(1000);
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found";
				getscreenshot();
			}
		}
		public void updateduserpassword() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).clear();
				Thread.sleep(1000);
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(updatepassword);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found";
				getscreenshot();
			}
		}
		public void terms_accept() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className(getObj().getProperty("term_accept"))));
				getDriver().findElement(By.className(getObj().getProperty("term_accept"))).click();
				getLogger().info("Agree Terms and Condition");
			}
			catch(Exception e)
			{
				TestReason = "Terms & Condition option not found";
				getLogger().info(TestReason);
				screenshotname = "Terms_Condition_Not_Found_TC_No_" + row;
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();			
			}
		}
		public void registerbutton() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("register_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("register_button"))).click();
				getLogger().info("Sign Up Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up button not found";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Button_Not_Found";
				getscreenshot();
				/*TestResult = "Fail";
				setregisterresult();*/
				Assert.fail();
			}	
		}
		public void myaccountlink() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.className(getObj().getProperty("my_account"))));
				getDriver().findElement(By.className(getObj().getProperty("my_account"))).click();
				getLogger().info("My Account Link Click");
			}
			catch (Exception e)
			{
				TestReason = "My Account Link not found";
				getLogger().info(TestReason);
				screenshotname = "My_Account_Link_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void accoutsettinglink() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("account_setting_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("account_setting_link"))).click();
				getLogger().info("Account Setting Link Click");
			} catch (Exception e) {
				TestReason = "Account Setting Link not found";
				getLogger().info(TestReason);
				screenshotname = "Account_Setting_Link_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void myprofielink() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("myprofielink"))));
				getDriver().findElement(By.xpath(getObj().getProperty("myprofielink"))).click();
				getLogger().info("My Profile Link Click");
			} catch (Exception e) {
				TestReason = "My Profile Link not found";
				getLogger().info(TestReason);
				screenshotname = "My_Profile_Link_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void logoutlink() throws Exception
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("logout"))));
				getDriver().findElement(By.xpath(getObj().getProperty("logout"))).click();
				Thread.sleep(2000);
				TestReason = "Register Successfully Done";
				getLogger().info(TestReason);
			}
			catch(Exception e)
			{
				TestReason = "Register not successfully done";
				getLogger().info(TestReason);
				screenshotname = "Register_Not_Success";
				getscreenshot();
				Assert.fail();
			}
		}
		public void registerclear() throws Exception
		{
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
			getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).clear();
			getDriver().findElement(By.xpath(getObj().getProperty("email"))).clear();
			getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).clear();
			getDriver().findElement(By.xpath(getObj().getProperty("password"))).clear();
		}	
	
//-----------------------------------------------Login---------------------------------------------------------------------
		

		public void login_button() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_home"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_home"))).click();
				getLogger().info("Login Button Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Button not load.";
				getLogger().info(TestReason);
				screenshotname = "Login_Button_Link_Not_Load";
				getscreenshot();
				Assert.fail();
			}
		}		
		public void login_now_button() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_now_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_now_button"))).click();
				getLogger().info("Login Now Button Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Now Button not load properly.";
				getLogger().info(TestReason);
				screenshotname = "Login_Now_Button_Not_Load";
				getscreenshot();		
			}
		}
		public void login_link() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_link"))).click();
				getLogger().info("Login Linked Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login link not load properly.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Load";
				getscreenshot();		
			}
		}
		public void forgot_pwd_link() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Forgot_pass_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Forgot_pass_link"))).click();
				getLogger().info("Forgot Password Link Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Forgot Password Link Not Found";
				getLogger().info(TestReason);
				screenshotname = "Forgot_Password_Link_Not_Found";
				getscreenshot();
			}
		}
		public void reset_pwd_button() throws Exception 
		{
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Reset_pass_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Reset_pass_button"))).click();
				getLogger().info("Reset Password Button Clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Reset Password Button not load properly.";
				getLogger().info(TestReason);
				screenshotname = "Reset_Password_Button_Not_Load";
				getscreenshot();		
			}
		}
		
//----------------------------------------Shipping Preference---------------------------------------------------	
	
		public void winepricerange() throws Exception
		{
			List<WebElement> priceranges = getDriver().findElements(By.className("optName"));
			for(WebElement pricerange:priceranges)
			{
			    if(price_range.contentEquals("$12 - $15")  && pricerange.getText().contentEquals("$12 - $15")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			    if(price_range.contentEquals("$15 - $30")  && pricerange.getText().contentEquals("$15 - $30")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			    if(price_range.contentEquals("$30 - $50")  && pricerange.getText().contentEquals("$30 - $50")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			    if(price_range.contentEquals("$75 - $100")  && pricerange.getText().contentEquals("$75 - $100")) 
			    {
			    	pricerange.click(); 
			        break;
			    }
			}
		}
		public void noofbottleship() throws Exception
		{
			List<WebElement> bottleships = getDriver().findElements(By.className("optName"));
			for(WebElement bottleship:bottleships)
			{								
			    if(bottle_ship.contentEquals("2")  && bottleship.getText().contentEquals("2 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("3")  && bottleship.getText().contentEquals("3 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("4")  && bottleship.getText().contentEquals("4 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("6")  && bottleship.getText().contentEquals("6 Bottles")) 
			    {
			    	bottleship.click(); 
			        break;
			    }
			}
		}
		public void nooftimeship() throws Exception
		{
			List<WebElement> noofbottles = getDriver().findElements(By.className("optName"));
			for(WebElement noofbottle:noofbottles)
			{								
			    if(bottle_ship.contentEquals("1")  && noofbottle.getText().contentEquals("1 time a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("2")  && noofbottle.getText().contentEquals("2 times a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("3")  && noofbottle.getText().contentEquals("3 times a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			    if(bottle_ship.contentEquals("4")  && noofbottle.getText().contentEquals("4 times a year")) 
			    {
			    	noofbottle.click(); 
			        break;
			    }
			}
		}
		public void ordertab() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("order_tab"))));
				getDriver().findElement(By.xpath(getObj().getProperty("order_tab"))).click();
				getLogger().info("Order Tab Option Click");
			} catch (Exception e) {
				TestReason = "Order Tab Option not found";
				getLogger().info(TestReason);
				screenshotname = "Order_Tab_Option_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void orderwinebutton() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("order_wine_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("order_wine_button"))).click();
				getLogger().info("Order Wine Button Click");
			} 
			catch (Exception e)
			{
				TestReason = "Order Wine Button not found";
				getLogger().info(TestReason);
				screenshotname = "Order_Wine_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void backtoorder() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("backtoorder"))));
				getDriver().findElement(By.xpath(getObj().getProperty("backtoorder"))).click();
				getLogger().info("Back to Order Button Click");
			} 
			catch (Exception e) 
			{
				TestReason = "Back to Order Button not found";
				getLogger().info(TestReason);
				screenshotname = "BAck_to_Order_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void addbottle1() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("add_bottle1"))));
				getDriver().findElement(By.xpath(getObj().getProperty("add_bottle1"))).click();
				getLogger().info("1st Bottle Added to Cart");
			} catch (Exception e) {
				TestReason = "1st Bottle not found";
				getLogger().info(TestReason);
				screenshotname = "1st_Bottle_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void addbottle2() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("add_bottle2"))));
				getDriver().findElement(By.xpath(getObj().getProperty("add_bottle2"))).click();
				getLogger().info("2nd Bottle Added to Cart");
			} catch (Exception e) {
				TestReason = "2nd Bottle not found";
				getLogger().info(TestReason);
				screenshotname = "2nd_Bottle_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void placeorderbutton() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("place_order_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("place_order_button"))).click();
				getLogger().info("Order Placed Button Clicked");
			} catch (Exception e) {
				TestReason = "Order Placed button not found";
				getLogger().info(TestReason);
				screenshotname = "Order_Placed_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
		public void replacecard() throws Exception
		{
			try 
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("replacecard_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("replacecard_button"))).click();
				getLogger().info("Replace Card Button Clicked");
			} catch (Exception e) {
				TestReason = "Replace card button not found";
				getLogger().info(TestReason);
				screenshotname = "Replace_Card_Button_Not_Found";
				getscreenshot();
				Assert.fail();
			}
		}
}
